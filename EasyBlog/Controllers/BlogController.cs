﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EasyBlog.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;

namespace EasyBlog.Controllers
{
    [Authorize]
    public class BlogController : Controller
    {
        private readonly DbXlp db;

        public BlogController(DbXlp context)
        {
            db = context;
        }

        // GET: InfoBlogs
        public async Task<IActionResult> Index()
        {
            return View(await db.InfoBlogs.Where(ii => ii.UserName == User.Identity.Name).ToListAsync());
        }

        // GET: InfoBlogs/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: InfoBlogs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,UserName,Title,Content,VisitCount,DTCreate")] InfoBlog infoBlog)
        {
            if (ModelState.IsValid)
            {
                infoBlog.UserName = User.Identity.Name;
                infoBlog.DTCreate = DateTime.Now;
                infoBlog.VisitCount = 0;
                db.Add(infoBlog);
                await db.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(infoBlog);
        }

        // GET: InfoBlogs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var infoBlog = await db.InfoBlogs.FindAsync(id);
            if (infoBlog == null)
            {
                return NotFound();
            }
            if (infoBlog.UserName != User.Identity.Name)
            {
                return NotFound();
            }

            return View(infoBlog);
        }

        // POST: InfoBlogs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,Title,Content")] InfoBlog infoBlog)
        {
            if (id != infoBlog.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {

                    infoBlog.UserName = User.Identity.Name;
                    infoBlog.DTCreate = DateTime.Now;
                    infoBlog.VisitCount = 0;

                    db.Update(infoBlog);
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!InfoBlogExists(infoBlog.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(infoBlog);
        }

        // GET: InfoBlogs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var infoBlog = await db.InfoBlogs
                .FirstOrDefaultAsync(m => m.ID == id);
            if (infoBlog == null)
            {
                return NotFound();
            }
            if (infoBlog.UserName != User.Identity.Name)
                return NotFound();

            return View(infoBlog);
        }

        // POST: InfoBlogs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var infoBlog = await db.InfoBlogs.FindAsync(id);
            db.InfoBlogs.Remove(infoBlog);
            await db.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool InfoBlogExists(int id)
        {
            return db.InfoBlogs.Any(e => e.ID == id);
        }
        [HttpPost]
        public async Task<ActionResult> GetImg(string userName)
        {
            var item = db.InfoUsers.Find(userName);
            object Data =new {BaseData=Convert.ToBase64String(item.HeaderImg) };
            string json = JsonConvert.SerializeObject(Data);
            return Content(json);
        }
    }
}
