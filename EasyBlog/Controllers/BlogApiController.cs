﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EasyBlog.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace EasyBlog.Controllers
{
    [ApiController]
    public class BlogApiController : ControllerBase
    {
        private readonly static Dictionary<string, string> DTokenUserName = new Dictionary<string, string>();

        private readonly DbXlp db;

        public BlogApiController(DbXlp context)
        {
            db = context;
        }

        /// <summary>
        /// 获取令牌
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns>如果为空就是用户名或者密码错误</returns>
        [HttpGet]
        [Route("Api/GetToken")]
        public ReposneToken GetToken(string userName, string password)
        {
            if (string.IsNullOrEmpty(userName) || string.IsNullOrEmpty(password))
                return new ReposneToken { Code = false, Message = "用户名和密码不能为空" };
            var item = db.InfoUsers.Find(userName);
            //if(item!=null && item.Password==password)
            if (item?.Password == password)
            {
                var token = Guid.NewGuid().ToString();
                DTokenUserName.Add(token, userName);
                return new ReposneToken { Token = token };

            }
            else
                return new ReposneToken { Code = false, Message = "密码错误" };

        }

        [HttpGet]
        [Route("Api/GetList")]
        public ReponseList GetList(string token)
        {
            if (!DTokenUserName.ContainsKey(token))
                return new ReponseList { Code = false, Message = "Token不存在" };
            string userName = DTokenUserName[token];
            return new ReponseList { Items = db.InfoBlogs.Where(ii => ii.UserName == userName).ToList(), Code = true };
        }

        [HttpPost]
        [Route("Api/Add")]
        public ResponseBase Add(string token, [FromBody] InfoBlog item)
        {
            if (!DTokenUserName.ContainsKey(token))
                return new ResponseBase("Token不存在");

            string userName = DTokenUserName[token];
            item.UserName = userName;
            //InfoBlog item = new InfoBlog { UserName = userName, Title = title, Content = content, DTCreate = DateTime.Now, VisitCount = 0 };
            db.InfoBlogs.Add(item);
            db.SaveChanges();
            return new ResponseBase();
        }


        public class ResponseBase
        {
            public bool Code { get; set; } = true;
            public string Message { get; set; }
            public ResponseBase()
            {
            }
            public ResponseBase(string msg)
            {
                Code = false;
                Message = msg;

            }
        }

        public class ReponseList : ResponseBase
        {
            public List<InfoBlog> Items { get; set; }
        }
        public class ReposneToken : ResponseBase
        {
            public string Token { get; set; }
        }
    }
}
