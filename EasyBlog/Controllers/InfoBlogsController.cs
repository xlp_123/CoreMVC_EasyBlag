﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using EasyBlog.Models;

namespace EasyBlog.Controllers
{
    public class InfoBlogsController : Controller
    {
        private readonly DbXlp db;

        public InfoBlogsController(DbXlp context)
        {
            db = context;
        }

        // GET: InfoBlogs
        public async Task<IActionResult> Index()
        {
            return View(await db.InfoBlogs.ToListAsync());
        }

        // GET: InfoBlogs/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var infoBlog = await db.InfoBlogs
                .FirstOrDefaultAsync(m => m.ID == id);
            if (infoBlog == null)
            {
                return NotFound();
            }

            return View(infoBlog);
        }

        // GET: InfoBlogs/Create
        public IActionResult Create()
        {
            return View();
        }

        // POST: InfoBlogs/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create([Bind("ID,UserName,Title,Content,VisitCount,DTCreate")] InfoBlog infoBlog)
        {
            if (ModelState.IsValid)
            {
                db.Add(infoBlog);
                await db.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(infoBlog);
        }

        // GET: InfoBlogs/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var infoBlog = await db.InfoBlogs.FindAsync(id);
            if (infoBlog == null)
            {
                return NotFound();
            }
            return View(infoBlog);
        }

        // POST: InfoBlogs/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("ID,UserName,Title,Content,VisitCount,DTCreate")] InfoBlog infoBlog)
        {
            if (id != infoBlog.ID)
            {
                return NotFound();
            }

            if (ModelState.IsValid)
            {
                try
                {
                    db.Update(infoBlog);
                    await db.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!InfoBlogExists(infoBlog.ID))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Index));
            }
            return View(infoBlog);
        }

        // GET: InfoBlogs/Delete/5
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var infoBlog = await db.InfoBlogs
                .FirstOrDefaultAsync(m => m.ID == id);
            if (infoBlog == null)
            {
                return NotFound();
            }

            return View(infoBlog);
        }

        // POST: InfoBlogs/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var infoBlog = await db.InfoBlogs.FindAsync(id);
            db.InfoBlogs.Remove(infoBlog);
            await db.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        private bool InfoBlogExists(int id)
        {
            return db.InfoBlogs.Any(e => e.ID == id);
        }
    }
}
