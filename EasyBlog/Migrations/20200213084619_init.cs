﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace EasyBlog.Migrations
{
    public partial class init : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "InfoBlogs",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    UserName = table.Column<string>(nullable: true),
                    Title = table.Column<string>(nullable: true),
                    Content = table.Column<string>(nullable: true),
                    VisitCount = table.Column<int>(nullable: false),
                    DTCreate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InfoBlogs", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "InfoReplies",
                columns: table => new
                {
                    ID = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:Identity", "1, 1"),
                    BlogID = table.Column<int>(nullable: false),
                    UserName = table.Column<string>(nullable: true),
                    Content = table.Column<string>(nullable: true),
                    DTCreate = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_InfoReplies", x => x.ID);
                });

            migrationBuilder.CreateTable(
                name: "IofoUsers",
                columns: table => new
                {
                    UserName = table.Column<string>(maxLength: 10, nullable: false),
                    Password = table.Column<string>(maxLength: 16, nullable: false),
                    DTCreate = table.Column<DateTime>(nullable: false),
                    DTLogin = table.Column<DateTime>(nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_IofoUsers", x => x.UserName);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "InfoBlogs");

            migrationBuilder.DropTable(
                name: "InfoReplies");

            migrationBuilder.DropTable(
                name: "IofoUsers");
        }
    }
}
