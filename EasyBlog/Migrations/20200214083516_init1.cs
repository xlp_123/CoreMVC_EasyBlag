﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace EasyBlog.Migrations
{
    public partial class init1 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_IofoUsers",
                table: "IofoUsers");

            migrationBuilder.RenameTable(
                name: "IofoUsers",
                newName: "InfoUsers");

            migrationBuilder.AddPrimaryKey(
                name: "PK_InfoUsers",
                table: "InfoUsers",
                column: "UserName");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_InfoUsers",
                table: "InfoUsers");

            migrationBuilder.RenameTable(
                name: "InfoUsers",
                newName: "IofoUsers");

            migrationBuilder.AddPrimaryKey(
                name: "PK_IofoUsers",
                table: "IofoUsers",
                column: "UserName");
        }
    }
}
