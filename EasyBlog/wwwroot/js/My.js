﻿function CallAjax(url, para, fn) {

    $.ajax({
        url: url,
        type: "post",
        data: para,
        dataType: "json",
        traditional: true,
        success: function (ret) {
            HideLoading();

            if (ret.code === 0)
                ShowMessage("出错了：" + ret.message);

            else {
                if (fn)
                    fn(ret);
            }
        }, error: function (ret) {
            HideLoading();
            ShowMessage("出错了：" + "ret=" + ret.status + "," + ret.readyState + ",msg=" + msg + ",ex=" + ex);
        }
    });
}
function CallAjaxHtml(url, para, fn) {
    $.ajax({
        url: url,
        type: "get",
        data: para,
        dataType: "html",
        traditional: true,
        success: function (ret) {
            fn(ret);
        }, error: function (ret) {
            if (ret.responseText !== "") {
                ShowMessage(ret.responseText);
            }
            else
                ShowMessage(ret.status);
        }
    });
}

function ShowToast(message, second) {
    if (!message)
        message = "操作成功";
    var sec = 2000;
    if (second) {
        sec = second * 1000;
    }

    if ($("#toast").length === 0) {
        $("body").eq(0).append(`<div class="modal fade" id="toast" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered" role= "document">
<div class="modal-content">
<div class="modal-body text-center">
${message}
</div>
</div>
</div>
</div>`);

    }
    else {
        $("#toast").find("modal-body").html(message);
    }
    $("#toast").modal("show");
    $("#toast").unbind("click").click(function () {
        $(this).modal("hide");
    });
    var tt = setTimeout(function () {
        $("#toast").click();
    }, sec);
    return {
        on: function (cb) {
            if (cb && cb instanceof Function) {
                $("#toast").unbind("click").click(function () {
                    clearTimeout(tt);
                    $(this).modal("hide");
                    cb();
                });
            }
        }
    };
}
function ShowLoading() {

    if ($("#loadingToast").length === 0) {
        $("body").eq(0).append(`<div class="text-center"><div id="DivLoading" class="spinner-border text-primary" style="width: 3rem; height: 3rem;" role="status">
  <span class="sr-only">Loading...</span>
</div></div>`);

    }
    $("#DivLoading").show();
}
function HideLoading() {
    $("#DivLoading").hide();
}
function ShowConfirm(message) {
    if ($("#DivConfirm").length === 0) {
        $("body").eq(0).append(`<div class="modal fade" id="DivConfirm" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered" role= "document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title" id="exampleModalLongTitle">忆客科技确认信息</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
${message}
</div>
<div class="modal-footer">
<button type="button" class="btn btn-secondary" data-dismiss="modal">取消</button>
<button type="button" class="btn btn-info">确定</button>
</div>
</div>
</div>
</div >`);
    }
    else {
        $("#DivConfirm").find(".modal-body").html(message);
    }
    $("#DivConfirm").modal("show");

    return {
        on: function (cb) {
            $("#DivConfirm").find('.btn-info').unbind("click").click(function () {
                $("#DivConfirm").modal("hide");
                cb(true);
            });
            $("#DivConfirm").find('.btn-secondary').unbind("click").click(function () {
                $("#DivConfirm").modal("hide");
                cb(false);
            });
        }
    };
}
//function ShowToast(message) {

//    $("body").eq(0).append(`<div role="alert" aria-live="assertive" aria-atomic="true" class="toast" data-autohide="false">
//        < div class= "toast-header" >
//        <strong class="mr-auto">Bootstrap</strong>
//        <small>11 mins ago</small>
//        <button type="button" class="ml-2 mb-1 close" data-dismiss="toast" aria-label="Close">
//            <span aria-hidden="true">&times;</span>
//        </button>
//        </div >
//        <div class="toast-body">
//          ${message}
//    </div>
//    </div >`);
//    $('.toast').toast('show');
//}
function ShowMessage(message) {
    if ($("#DivMessage").length === 0) {
        $("body").eq(0).append(`<div class="modal fade" id="DivMessage" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
<div class="modal-dialog modal-dialog-centered" role= "document">
<div class="modal-content">
<div class="modal-header">
<h5 class="modal-title" id="exampleModalLongTitle">忆客科技提示信息</h5>
<button type="button" class="close" data-dismiss="modal" aria-label="Close">
<span aria-hidden="true">&times;</span>
</button>
</div>
<div class="modal-body">
${message}
</div>
<div class="modal-footer">
<button type="button" class="btn btn-info">确定</button>
</div>
</div>
</div>
</div >`);

    }
    else {
        $("#DivMessage").find(".modal-body").html(message);
    }
    $("#DivMessage").modal("show");

    $("#DivMessage").unbind("click").click(function () {
        $("#DivMessage").modal('hide');
    });
    return {
        on: function (cb) {
            if (cb && cb instanceof Function) {
                $("#DivMessage").unbind("click").click(function () {
                    $("#DivMessage").modal('hide');
                    cb();
                });
            }
        }
    };
}

