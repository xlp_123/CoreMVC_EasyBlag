﻿using EasyBlog.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyBlog.Data
{
    public class RequestItem
    {
        [JsonProperty("M")]
        public string MethodName { get; set; }
        [JsonProperty("C")]
        public string Content { get; set; }
        [JsonProperty("T")]
        public string Token { get; set; }
    }
    public class ResponseItem
    {
        public bool Code { get; set; } = true;
        public string Message { get; set; }
        public string Content { get; set; }
    }

    public class ResponseBase
    {
        public bool Code { get; set; } = true;
        public string Message { get; set; }
        public ResponseBase()
        {
        }
        public ResponseBase(string msg)
        {
            Code = false;
            Message = msg;

        }
    }

    public class ReponseList : ResponseBase
    {
        public List<InfoBlog> Items { get; set; }
    }
    public class ReposneToken : ResponseBase
    {
        public string Token { get; set; }
    }
}
