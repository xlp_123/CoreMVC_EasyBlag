﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EasyBlog.Models
{
    public class DbXlp : DbContext
    {
        public DbXlp(DbContextOptions<DbXlp> options)
         : base(options)
        {
       
        }
        //add-migration init update-database 
        public DbSet<InfoUser> InfoUsers { get; set; }
        public DbSet<InfoBlog> InfoBlogs { get; set; }
        public DbSet<InfoReply> InfoReplies { get; set; }
        public DbSet<InfoLog> InfoLogs { get; set; }

    }
}
