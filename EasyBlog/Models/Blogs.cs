﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace EasyBlog.Models
{
    /// <summary>
    /// 用户表
    /// </summary>
    public class InfoUser
    {
        [Key]
        [StringLength(10)]
        [DisplayName("用户名")]
        [Required(ErrorMessage ="用户名不能为空")]
        public string UserName { get; set; }
        [StringLength(16)]
        [DisplayName("密码")]
        [Required(ErrorMessage = "密码不能为空")]
        public string Password { get; set; }
        [DisplayName("创建日期")]
        public DateTime DTCreate { get; set; }
        [DisplayName("登陆日期")]
        public DateTime DTLogin { get; set; }

        public byte[] HeaderImg { get; set; }
    }
    /// <summary>
    /// 博客表
    /// </summary>
    public class InfoBlog
    {
        public int ID { get; set; }
        [StringLength(10)]
        public string UserName { get; set; }
        [DisplayName("标题")]
        [StringLength(64)]
        public string Title { get; set; }
        [DisplayName("内容")]
        public string Content { get; set; }
        [DisplayName("浏览次数")]
        public int VisitCount { get; set; }
        [DisplayName("创建日期")]
        public DateTime DTCreate { get; set; }
    }
    /// <summary>
    /// 回复表
    /// </summary>
    public class InfoReply
    {
        public int ID { get; set; }
        public int BlogID { get; set; }
        [StringLength(10)]
        public string UserName { get; set; }
        public string Content { get; set; }
        public DateTime DTCreate { get; set; }
    }
    /// <summary>
    /// 日志表
    /// </summary>
    public class InfoLog
    {
        public int ID { get; set; }
        [StringLength(64)]
        public string Title { get; set; }
        public string Content { get; set; }
        public DateTime DTCerate { get; set; }

    }
}
